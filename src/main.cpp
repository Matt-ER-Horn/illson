// Illson or ShittyChemspeed -  The ultra-budget liquid handler
//  Based on the Creality Ender Pro 2 and the Fysetc_E4 Control board.
#include <Arduino.h>
#include <TMCStepper.h>
#include <AccelStepper.h>



// Stepper Driver Setup for TMCStepper library
// note: needle (Extruder) and x are reversed from fysetc documentation
// note: z and y are reversed from fysetc documentation

#define EN_PIN 25       // Enable

#define needle_DIR_PIN 26          // Direction
#define needle_STEP_PIN 27         // Step
#define needle_DRIVER_ADDRESS 0b01 // TMC2209 Driver address according to MS1 and MS2
#define needle_DIAG 34             // sensorless homing pin

#define y_DIR_PIN 32          // Direction
#define y_STEP_PIN 33         // Step
#define y_DRIVER_ADDRESS 0b00 // TMC2209 Driver address according to MS1 and MS2
#define y_DIAG 35             // sensorless homing pin

#define z_DIR_PIN 12          // Direction
#define z_STEP_PIN 14         // Step
#define z_DRIVER_ADDRESS 0b10 // TMC2209 Driver address according to MS1 and MS2
#define z_DIAG 15             // sensorless homing pin

#define x_DIR_PIN 17          // Direction
#define x_STEP_PIN 16         // Step
#define x_DRIVER_ADDRESS 0b11 // TMC2209 Driver address according to MS1 and MS2

#define RXD2 21
#define TXD2 22
#define SERIAL_PORT Serial2 // TMC2208/TMC2224 HardwareSerial port
#define R_SENSE 0.22f       // Match to your driver
TMC2209Stepper x_driver(&SERIAL_PORT, R_SENSE, x_DRIVER_ADDRESS);
TMC2209Stepper y_driver(&SERIAL_PORT, R_SENSE, y_DRIVER_ADDRESS);
TMC2209Stepper z_driver(&SERIAL_PORT, R_SENSE, z_DRIVER_ADDRESS);
TMC2209Stepper needle_driver(&SERIAL_PORT, R_SENSE, needle_DRIVER_ADDRESS);

// Define Steppers accelstepper
AccelStepper stepper_x = AccelStepper(stepper_x.DRIVER, x_STEP_PIN, x_DIR_PIN);
AccelStepper stepper_y = AccelStepper(stepper_y.DRIVER, y_STEP_PIN, y_DIR_PIN);
AccelStepper stepper_z = AccelStepper(stepper_z.DRIVER, z_STEP_PIN, z_DIR_PIN);
AccelStepper stepper_needle = AccelStepper(stepper_needle.DRIVER, needle_STEP_PIN, needle_DIR_PIN);

// Variables for stepper motors
int maxSpeed_x = 3000;
int acceleration_x = 5000;

int maxSpeed_y = 3000;
int acceleration_y = 5000;

int maxSpeed_z = 2000;
int acceleration_z = 5000;

int maxSpeed_needle = 2000;
int acceleration_needle = 4000;

int highCurrent = 1100;
int lowCurrent = 650;
int stallValue = 160;


//Variables
float xCal = 10;
int xSteps;
float xMax = 150;

float yCal = 10;
int ySteps;
float yMax = 150;

float zCal = 50;
int zSteps;
float zMax = 220;

float nCal = 205;
int nSteps;
float nMax = 30;



// Serial Communication

char buff[32];
char command[3];
volatile long cmd_value = 0;
#define CMD_LEN 2
bool currentlyHoming = false;



void homeN()
{
     {   
//stepper_needle.setMaxSpeed(homingSpeed);
  needle_driver.rms_current(highCurrent);
  stepper_needle.setMaxSpeed(1000);
  needle_driver.microsteps(0);
  needle_driver.SGTHRS(stallValue); 
  // digitalWrite(EN_PIN, HIGH);//make sure error condition is reset 
  // delay(500);
  // digitalWrite(EN_PIN, LOW);
  // delay(500); 
  stepper_needle.setCurrentPosition(0);
  stepper_needle.move(200); // move away from home position to make sure there is space to home
  while(stepper_needle.distanceToGo() > 0){
    stepper_needle.run();
  }
  stepper_needle.move(-400000);
  while (stepper_needle.currentPosition() != 0){ //run 200 steps to get stepper up to speed before the sensorless homing is activated
  stepper_needle.run();
  //Serial.println(digitalRead(STALL_PIN_CLAW)); //debug SG_Result
  //Serial.println(needle_driver.SG_RESULT());
  }
  while(digitalRead(needle_DIAG) == LOW) { //activate sensorless homing - TODO do this as an interupt 
    stepper_needle.run();
    //  static uint32_t last_time=0;
    //  uint32_t ms = millis();
    //  stepper.run();
    //  if((ms-last_time) > 100) { //run every 0.1s
    //  last_time = ms;
    //  Serial.println(driver.SG_RESULT());
    //  }

  }
  // stepper_needle.setCurrentPosition(0);
  // stepper_needle.move(10); // move away from home position to make sure there is space to home
  // while(stepper_needle.distanceToGo() != 0){
  //   stepper_needle.run();
  // }
 stepper_needle.setCurrentPosition(0);
 needle_driver.rms_current(lowCurrent);
 needle_driver.microsteps(2);
 stepper_needle.setMaxSpeed(maxSpeed_needle);
 Serial.println("Done Homing Needle");
 }
}

void homeZ()
{
     {   
//stepper_needle.setMaxSpeed(homingSpeed);
  z_driver.rms_current(highCurrent);
  stepper_z.setMaxSpeed(1000);
  z_driver.microsteps(0);
  z_driver.SGTHRS(stallValue); 
  // digitalWrite(EN_PIN, HIGH);//make sure error condition is reset 
  // delay(500);
  // digitalWrite(EN_PIN, LOW);
  // delay(500); 
  stepper_z.setCurrentPosition(0);
  stepper_z.move(200); // move away from home position to make sure there is space to home
  while(stepper_z.distanceToGo() > 0){
    stepper_z.run();
  }
  stepper_z.move(-400000);
  while (stepper_z.currentPosition() != 0){ //run 200 steps to get stepper up to speed before the sensorless homing is activated
  stepper_z.run();
  //Serial.println(digitalRead(STALL_PIN_CLAW)); //debug SG_Result
  //Serial.println(needle_driver.SG_RESULT());
  }
  while(digitalRead(z_DIAG) == LOW) { //activate sensorless homing - TODO do this as an interupt 
    stepper_z.run();
    //  static uint32_t last_time=0;
    //  uint32_t ms = millis();
    //  stepper.run();
    //  if((ms-last_time) > 100) { //run every 0.1s
    //  last_time = ms;
    //  Serial.println(driver.SG_RESULT());
    //  }

  }
  // stepper_needle.setCurrentPosition(0);
  // stepper_needle.move(10); // move away from home position to make sure there is space to home
  // while(stepper_needle.distanceToGo() != 0){
  //   stepper_needle.run();
  // }
 stepper_z.setCurrentPosition(0);
 z_driver.rms_current(lowCurrent);
 z_driver.microsteps(2);
 stepper_z.setMaxSpeed(maxSpeed_z);
 Serial.println("Done Homing Z");
 }
}

void homeX()
{  
//stepper_needle.setMaxSpeed(homingSpeed);
  x_driver.rms_current(highCurrent);
  stepper_x.setMaxSpeed(500);
  x_driver.microsteps(0); 
  // digitalWrite(EN_PIN, HIGH);//make sure error condition is reset 
  // delay(500);
  // digitalWrite(EN_PIN, LOW);
  // delay(500); 
  stepper_x.setCurrentPosition(0);
  stepper_x.move(20); // move away from home position to make sure there is space to home
  while(stepper_x.distanceToGo() > 0){
    stepper_x.run();
  }
  stepper_x.move(-400000);
  while (stepper_x.currentPosition() != 0){ //run 200 steps to get stepper up to speed before the sensorless homing is activated
  stepper_x.run();
  //Serial.println(digitalRead(STALL_PIN_CLAW)); //debug SG_Result
  //Serial.println(needle_driver.SG_RESULT());
  }
  while(digitalRead(y_DIAG) == HIGH) { //activate sensorless homing - TODO do this as an interupt 
    stepper_x.run();
    //  static uint32_t last_time=0;
    //  uint32_t ms = millis();
    //  stepper.run();
    //  if((ms-last_time) > 100) { //run every 0.1s
    //  last_time = ms;
    //  Serial.println(driver.SG_RESULT());
    //  }

  }
  stepper_x.move(10); // move away from home position clear limit switch for shared ciruit
  while(stepper_x.distanceToGo() != 0){
    stepper_x.run();
  }
 stepper_x.setCurrentPosition(0);
 x_driver.rms_current(lowCurrent);
 x_driver.microsteps(2);
 stepper_x.setMaxSpeed(maxSpeed_x);
 Serial.println("Done Homing X");
 }

void homeY()
{  
//stepper_needle.setMaxSpeed(homingSpeed);
  y_driver.rms_current(highCurrent);
  stepper_y.setMaxSpeed(500);
  y_driver.microsteps(0);  
  stepper_y.setCurrentPosition(0);
  stepper_y.move(20); // move away from home position to make sure there is space to home
  while(stepper_y.distanceToGo() > 0){
    stepper_y.run();
  }
  stepper_y.move(-400000);
  while (stepper_y.currentPosition() != 0){ //run 200 steps to get stepper up to speed before the sensorless homing is activated
  stepper_y.run();
  }
  while(digitalRead(y_DIAG) == HIGH) { //activate sensorless homing - TODO do this as an interupt 
    stepper_y.run();

  }
 
  stepper_y.move(10); // move away from home to release shared limit switch circuit
  while(stepper_y.distanceToGo() != 0){
  stepper_y.run();
  }
 stepper_y.setCurrentPosition(0);
 y_driver.rms_current(lowCurrent);
 y_driver.microsteps(2);
 stepper_y.setMaxSpeed(maxSpeed_y);
 Serial.println("Done Homing Y");
 }




void setup()
{
 Serial.begin(115200);
 Serial2.begin(115200, SERIAL_8N1, RXD2, TXD2);

 // Set up for the TMCStepper library and AccelStepper library
 // Stepper driver setup------------------------------------
 x_driver.begin();
 x_driver.toff(1); // 4   // Sets the slow decay time (off time) [1... 15]. This setting also limits the maximum chopper frequency. For operation with StealthChop, this parameter is not used, but it is required to enable the motor. In case of operation with StealthChop only, any setting is OK.
 // driver.VACTUAL(speed); // VACTUAL allows moving the motor by UART control. It gives the motor velocity in +-(2^23)-1 [μsteps / t] 0: Normal operation. Driver reacts to STEP input./=0: Motor moves with the velocity given by VACTUAL. Step pulses can be monitored via INDEX output. The motor direction is controlled by the sign of VACTUAL.
 x_driver.blank_time(24);          // Comparator blank time. This time needs to safely cover the switching event and the duration of the ringing on the sense resistor. For most applications, a setting of 16 or 24 is good. For highly capacitive loads, a setting of 32 or 40 will be required.
 x_driver.rms_current(lowCurrent); // mA
 x_driver.microsteps(2);
 x_driver.TCOOLTHRS(0xFFFFF); // 20bit max // Lower threshold velocity for switching on smart energy CoolStep and StallGuard to DIAG output
 x_driver.semin(0);           // CoolStep lower threshold [0... 15]. If SG_RESULT goes below this threshold, CoolStep increases the current to both coils.  0: disable CoolStep
 x_driver.semax(2);           // CoolStep upper threshold [0... 15].  If SG is sampled equal to or above this threshold enough times, CoolStep decreases the current to both coils.
 x_driver.sedn(0b01);         // Sets the number of StallGuard2 readings above the upper threshold necessary for each current decrement of the motor current.
 x_driver.SGTHRS(stallValue); // StallGuard4 threshold [0... 255] level for stall detection. It compensates for motor specific characteristics and controls sensitivity. A higher value gives a higher sensitivity. A higher value makes StallGuard4 more sensitive and requires less torque to indicate a stall. The double of this value is compared to SG_RESULT. The stall output becomes active if SG_RESULT fall below this value.

 y_driver.begin();
 y_driver.toff(1); // 4   // Sets the slow decay time (off time) [1... 15]. This setting also limits the maximum chopper frequency. For operation with StealthChop, this parameter is not used, but it is required to enable the motor. In case of operation with StealthChop only, any setting is OK.
 // driver.VACTUAL(speed); // VACTUAL allows moving the motor by UART control. It gives the motor velocity in +-(2^23)-1 [μsteps / t] 0: Normal operation. Driver reacts to STEP input./=0: Motor moves with the velocity given by VACTUAL. Step pulses can be monitored via INDEX output. The motor direction is controlled by the sign of VACTUAL.
 y_driver.blank_time(24);          // Comparator blank time. This time needs to safely cover the switching event and the duration of the ringing on the sense resistor. For most applications, a setting of 16 or 24 is good. For highly capacitive loads, a setting of 32 or 40 will be required.
 y_driver.rms_current(lowCurrent); // mA
 y_driver.microsteps(2);
 y_driver.TCOOLTHRS(0xFFFFF); // 20bit max // Lower threshold velocity for switching on smart energy CoolStep and StallGuard to DIAG output
 y_driver.semin(0);           // CoolStep lower threshold [0... 15]. If SG_RESULT goes below this threshold, CoolStep increases the current to both coils.  0: disable CoolStep
 y_driver.semax(2);           // CoolStep upper threshold [0... 15].  If SG is sampled equal to or above this threshold enough times, CoolStep decreases the current to both coils.
 y_driver.sedn(0b01);         // Sets the number of StallGuard2 readings above the upper threshold necessary for each current decrement of the motor current.
 y_driver.SGTHRS(stallValue); // StallGuard4 threshold [0... 255] level for stall detection. It compensates for motor specific characteristics and controls sensitivity. A higher value gives a higher sensitivity. A higher value makes StallGuard4 more sensitive and requires less torque to indicate a stall. The double of this value is compared to SG_RESULT. The stall output becomes active if SG_RESULT fall below this value.

 z_driver.begin();
 z_driver.toff(1); // 4   // Sets the slow decay time (off time) [1... 15]. This setting also limits the maximum chopper frequency. For operation with StealthChop, this parameter is not used, but it is required to enable the motor. In case of operation with StealthChop only, any setting is OK.
 // driver.VACTUAL(speed); // VACTUAL allows moving the motor by UART control. It gives the motor velocity in +-(2^23)-1 [μsteps / t] 0: Normal operation. Driver reacts to STEP input./=0: Motor moves with the velocity given by VACTUAL. Step pulses can be monitored via INDEX output. The motor direction is controlled by the sign of VACTUAL.
 z_driver.blank_time(24);          // Comparator blank time. This time needs to safely cover the switching event and the duration of the ringing on the sense resistor. For most applications, a setting of 16 or 24 is good. For highly capacitive loads, a setting of 32 or 40 will be required.
 z_driver.rms_current(lowCurrent); // mA
 z_driver.microsteps(2);
 z_driver.TCOOLTHRS(0xFFFFF); // 20bit max // Lower threshold velocity for switching on smart energy CoolStep and StallGuard to DIAG output
 z_driver.semin(0);           // CoolStep lower threshold [0... 15]. If SG_RESULT goes below this threshold, CoolStep increases the current to both coils.  0: disable CoolStep
 z_driver.semax(2);           // CoolStep upper threshold [0... 15].  If SG is sampled equal to or above this threshold enough times, CoolStep decreases the current to both coils.
 z_driver.sedn(0b01);         // Sets the number of StallGuard2 readings above the upper threshold necessary for each current decrement of the motor current.
 z_driver.SGTHRS(stallValue); // StallGuard4 threshold [0... 255] level for stall detection. It compensates for motor specific characteristics and controls sensitivity. A higher value gives a higher sensitivity. A higher value makes StallGuard4 more sensitive and requires less torque to indicate a stall. The double of this value is compared to SG_RESULT. The stall output becomes active if SG_RESULT fall below this value.

 needle_driver.begin();
 needle_driver.toff(1); // 4   // Sets the slow decay time (off time) [1... 15]. This setting also limits the maximum chopper frequency. For operation with StealthChop, this parameter is not used, but it is required to enable the motor. In case of operation with StealthChop only, any setting is OK.
 // driver.VACTUAL(speed); // VACTUAL allows moving the motor by UART control. It gives the motor velocity in +-(2^23)-1 [μsteps / t] 0: Normal operation. Driver reacts to STEP input./=0: Motor moves with the velocity given by VACTUAL. Step pulses can be monitored via INDEX output. The motor direction is controlled by the sign of VACTUAL.
 needle_driver.blank_time(24);          // Comparator blank time. This time needs to safely cover the switching event and the duration of the ringing on the sense resistor. For most applications, a setting of 16 or 24 is good. For highly capacitive loads, a setting of 32 or 40 will be required.
 needle_driver.rms_current(lowCurrent); // mA
 needle_driver.microsteps(2);
 needle_driver.TCOOLTHRS(0xFFFFF); // 20bit max // Lower threshold velocity for switching on smart energy CoolStep and StallGuard to DIAG output
 needle_driver.semin(0);           // CoolStep lower threshold [0... 15]. If SG_RESULT goes below this threshold, CoolStep increases the current to both coils.  0: disable CoolStep
 needle_driver.semax(2);           // CoolStep upper threshold [0... 15].  If SG is sampled equal to or above this threshold enough times, CoolStep decreases the current to both coils.
 needle_driver.sedn(0b01);         // Sets the number of StallGuard2 readings above the upper threshold necessary for each current decrement of the motor current.
 needle_driver.SGTHRS(stallValue); // StallGuard4 threshold [0... 255] level for stall detection. It compensates for motor specific characteristics and controls sensitivity. A higher value gives a higher sensitivity. A higher value makes StallGuard4 more sensitive and requires less torque to indicate a stall. The double of this value is compared to SG_RESULT. The stall output becomes active if SG_RESULT fall below this value.

 stepper_x.setMaxSpeed(maxSpeed_x);         // 100mm/s @ 80 steps/mm
 stepper_x.setAcceleration(acceleration_x); // 2000mm/s^2
 stepper_x.setEnablePin(EN_PIN);
 stepper_x.setPinsInverted(true, false, true);
 stepper_x.enableOutputs();

 stepper_y.setMaxSpeed(maxSpeed_y);         // 100mm/s @ 80 steps/mm
 stepper_y.setAcceleration(acceleration_y); // 2000mm/s^2
 stepper_y.setEnablePin(EN_PIN);
 stepper_y.setPinsInverted(true, false, true);
 stepper_y.enableOutputs();

 stepper_z.setMaxSpeed(maxSpeed_z);         // 100mm/s @ 80 steps/mm
 stepper_z.setAcceleration(acceleration_z); // 2000mm/s^2
 stepper_z.setEnablePin(EN_PIN);
 stepper_z.setPinsInverted(true, false, true);
 stepper_z.enableOutputs();

 stepper_needle.setMaxSpeed(maxSpeed_needle);         // 100mm/s @ 80 steps/mm
 stepper_needle.setAcceleration(acceleration_needle); // 2000mm/s^2
 stepper_needle.setEnablePin(EN_PIN);
 stepper_needle.setPinsInverted(true, false, true);
 stepper_needle.enableOutputs();

 // setup stall pin to read config pin for sensorless homing----------
 pinMode(needle_DIAG, INPUT);
 pinMode(z_DIAG, INPUT);
 pinMode(y_DIAG, INPUT);


 pinMode(EN_PIN, OUTPUT);

 pinMode(x_STEP_PIN, OUTPUT);
 pinMode(x_DIR_PIN, OUTPUT);

 pinMode(y_STEP_PIN, OUTPUT);
 pinMode(y_DIR_PIN, OUTPUT);

 pinMode(z_STEP_PIN, OUTPUT);
 pinMode(z_DIR_PIN, OUTPUT);

 pinMode(needle_STEP_PIN, OUTPUT);
 pinMode(needle_DIR_PIN, OUTPUT);

 digitalWrite(EN_PIN, HIGH);
 digitalWrite(EN_PIN, LOW);

}

void loop()
{

  if (Serial.available() > 0)
  {
    uint8_t num_chars = Serial.readBytesUntil(0x0D,buff, sizeof(buff)); // Reads until it reaches terminal carriage return  
    buff[num_chars] = '\0';
    sscanf(buff, "%2s %li", command, &cmd_value);
    command[2] = '\0';

    if (strncmp(command, "xm", CMD_LEN) == 0)
    { // Set new target in steps
      if (stepper_x.distanceToGo() != 0 || currentlyHoming == true)
      {
        Serial.println("-3");
      }
      else
      {
        Serial.println("-1");
        x_driver.rms_current(highCurrent);
        xSteps = (cmd_value * xCal); // converts the entered mm value to steps
        stepper_x.move(xSteps);
        while (stepper_x.distanceToGo() != 0)
        {
          stepper_x.run();
        }
        x_driver.rms_current(lowCurrent);
        Serial.println("-4");
      }
    }
    
    else if (strncmp(command, "nm", CMD_LEN) == 0)
    { // Set new target in steps
      if (stepper_needle.distanceToGo() != 0 || currentlyHoming == true)
      {
        Serial.println("-3");
      }
      else
      {
        Serial.println("-1");
        needle_driver.rms_current(highCurrent);
        nSteps = (cmd_value * nCal); // converts the entered mm value to steps
        stepper_needle.move(nSteps);
        while (stepper_needle.distanceToGo() != 0)
        {
          stepper_needle.run();
        }
        needle_driver.rms_current(lowCurrent);
        Serial.println("-4");
      }
    }

    else if (strncmp(command, "ym", CMD_LEN) == 0)
    { // Set new target in steps
      if (stepper_y.distanceToGo() != 0 || currentlyHoming == true)
      {
        Serial.println("-3");
      }
      else
      {
        Serial.println("-1");
        y_driver.rms_current(highCurrent);
        ySteps = (cmd_value * yCal); // converts the entered mm value to steps
        stepper_y.move(ySteps);
        while (stepper_y.distanceToGo() != 0)
        {
          stepper_y.run();
        }
        y_driver.rms_current(lowCurrent);
        Serial.println("-4");
      }
    }

    else if (strncmp(command, "zm", CMD_LEN) == 0)
    { // Set new target in steps
      if (stepper_z.distanceToGo() != 0 || currentlyHoming == true)
      {
        Serial.println("-3");
      }
      else
      {
        Serial.println("-1");
        z_driver.rms_current(highCurrent);
        zSteps = (cmd_value * zCal); // converts the entered mm value to steps
        stepper_z.move(zSteps);
        while (stepper_z.distanceToGo() != 0)
        {
          stepper_z.run();
        }
        z_driver.rms_current(lowCurrent);
        Serial.println("-4");
      }
    }

    else if (strncmp(command, "xt", CMD_LEN) == 0)
    { // Set new target in steps
      if (stepper_x.distanceToGo() != 0 || currentlyHoming == true || cmd_value < 0 || cmd_value > xMax)
      {
        Serial.println("-3");
      }
    else
      {
        Serial.println("-1");
        x_driver.rms_current(highCurrent);
        xSteps = (cmd_value * xCal); // converts the entered mm value to steps
        stepper_x.moveTo(xSteps);
        while (stepper_x.distanceToGo() != 0)
        {
          stepper_x.run();
        }
        x_driver.rms_current(lowCurrent);
        Serial.println("-4");
      }
    }

    else if (strncmp(command, "nt", CMD_LEN) == 0)
    { // Set new target in steps
      if (stepper_needle.distanceToGo() != 0 || currentlyHoming == true || cmd_value < 0 || cmd_value > nMax)
      {
        Serial.println("-3");
      }
      else
      {
        Serial.println("-1");
        needle_driver.rms_current(highCurrent);
        nSteps = (cmd_value * nCal); // converts the entered mm value to steps
        stepper_needle.moveTo(nSteps);
        while (stepper_needle.distanceToGo() != 0)
        {
          stepper_needle.run();
        }
        needle_driver.rms_current(lowCurrent);
        Serial.println("-4");
      }
    }

    else if (strncmp(command, "yt", CMD_LEN) == 0)
    { // Set new target in steps
      if (stepper_y.distanceToGo() != 0 || currentlyHoming == true || cmd_value < 0 || cmd_value > yMax)
      {
        Serial.println("-3");
      }
      else
      {
        Serial.println("-1");
        y_driver.rms_current(highCurrent);
        ySteps = (cmd_value * yCal); // converts the entered mm value to steps
        stepper_y.moveTo(ySteps);
        while (stepper_y.distanceToGo() != 0)
        {
          stepper_y.run();
        }
        y_driver.rms_current(lowCurrent);
        Serial.println("-4");
      }
    }

    else if (strncmp(command, "zt", CMD_LEN) == 0)
    { // Set new target in steps
      if (stepper_z.distanceToGo() != 0 || currentlyHoming == true || cmd_value < 0 || cmd_value > zMax)
      {
        Serial.println("-3");
      }
      else
      {
        Serial.println("-1");
        z_driver.rms_current(highCurrent);
        zSteps = (cmd_value * zCal); // converts the entered mm value to steps
        stepper_z.moveTo(zSteps);
        while (stepper_z.distanceToGo() != 0)
        {
          stepper_z.run();
        }
        z_driver.rms_current(lowCurrent);
        Serial.println("-4");
      }
    }

    else if (strncmp(command, "xd", CMD_LEN) == 0)
    { // Query for current position relative to target position
      Serial.println("-1");
      Serial.println(stepper_x.distanceToGo());
      Serial.println("-4");
    }

    else if (strncmp(command, "np", CMD_LEN) == 0)
    { // Query for target position
      Serial.println("-1");
      int needlePos = stepper_needle.currentPosition() * nCal;
      Serial.println(needlePos);
      Serial.println("-4");
    }

    else if (strncmp(command, "zp", CMD_LEN) == 0)
    { // Query for target position
      Serial.println("-1");
      int zPos = stepper_z.currentPosition() * zCal;
      Serial.println(zPos);
      Serial.println("-4");
    }

    else if (strncmp(command, "xs", CMD_LEN) == 0)
    { // Stops the motor with decleration
      Serial.println("-1");
      stepper_x.stop();
      while (stepper_x.distanceToGo() != 0)
      {
        stepper_x.run();
      }
      Serial.println("-4");
    }

    else if (strncmp(command, "nh", CMD_LEN) == 0)
    { // Home X axis
      if (stepper_needle.distanceToGo() != 0)
      {
        Serial.println("-3");
      }
      else
      {
        Serial.println("-1");
        homeN();
        Serial.println("-4");
      }
    }

    else if (strncmp(command, "zh", CMD_LEN) == 0)
    { // Home X axis
      if (stepper_z.distanceToGo() != 0)
      {
        Serial.println("-3");
      }
      else
      {
        Serial.println("-1");
        homeZ();
        Serial.println("-4");
      }
    }

    else if (strncmp(command, "xh", CMD_LEN) == 0)
    { // Home X axis
      if (stepper_x.distanceToGo() != 0)
      {
        Serial.println("-3");
      }
      else
      {
        Serial.println("-1");
        homeX();
        Serial.println("-4");
      }
    }

    else if (strncmp(command, "yh", CMD_LEN) == 0)
    { // Home X axis
      if (stepper_y.distanceToGo() != 0)
      {
        Serial.println("-3");
      }
      else
      {
        Serial.println("-1");
        homeY();
        Serial.println("-4");
      }
    }

        else if (strncmp(command, "ah", CMD_LEN) == 0)
    { // Home X axis
      if (stepper_y.distanceToGo() != 0)
      {
        Serial.println("-3");
      }
      else
      {
        Serial.println("-1");
        // home Z first if it gets stuck somewhere
        homeZ();
        homeN();
        homeY();
        homeX();
        Serial.println("-4");
      }
    }

    else if (strncmp(command, "vs", CMD_LEN) == 0)
    { 
      if (stepper_x.distanceToGo() != 0)
      {
        Serial.println("-3");
      }
      else
      {
        Serial.println("-1");
        stallValue = cmd_value;
        Serial.println(stallValue);
        Serial.println("-4");
      }
    }
    else
    {
      Serial.println("-2");
    }
  }
  // stepper_x.run();
}